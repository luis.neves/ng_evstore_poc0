
name := "bs-poc0"
version := "0.1"
scalaVersion := "2.12.17"

resolvers += Resolver.mavenLocal

//libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.11"
//libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % "test"
libraryDependencies += "commons-validator" % "commons-validator" % "1.7"
libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.12.0"
libraryDependencies += "com.github.seancfoley" % "ipaddress" % "5.4.0"


libraryDependencies += "org.apache.spark" %% "spark-avro" % "3.3.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-hadoop-cloud" % "3.3.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.3.1" % "provided"
libraryDependencies += "org.apache.iceberg" %% "iceberg-spark-runtime-3.3" % "1.1.0" % "provided"

Compile / unmanagedSourceDirectories := (Compile / scalaSource).value :: Nil
Test / unmanagedSourceDirectories := (Test / scalaSource).value :: Nil

assemblyPackageScala / assembleArtifact := true
assembly / assemblyJarName := "bs-poc0-spark.jar"

//assembly / mainClass := Some("app.Main")

assembly / assemblyMergeStrategy := {
  case PathList("META-INF","services", xg @ _*) => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}