package app

import org.apache.spark.sql.SparkSession

object ComputeSparkCatalog {

  val warehousePath: String = "s3://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ng_event_store/"

  lazy val sparkSession = {
    SparkSession.builder()
      .appName("test")
      .config("spark.hadoop.fs.s3.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
      .config("spark.hadoop.fs.s3a.path.style.access", "true")
      .config("spark.sql.caseSensitive", true)
      .config("spark.sql.extensions", "org.apache.iceberg.spark.extensions.IcebergSparkSessionExtensions")
      .config("spark.sql.catalog.spark_catalog", "org.apache.iceberg.spark.SparkSessionCatalog")
      .config("spark.sql.catalog.spark_catalog.type", "hadoop")
      .config("spark.sql.catalog.spark_catalog.warehouse", warehousePath)
      //.config("spark.sql.catalog.spark_catalog.catalog-impl", "org.apache.iceberg.hadoop.HadoopCatalog")
      //.config("spark.sql.warehouse.dir", warehousePath)
      .getOrCreate()
  }

  def withSpark(stop: Boolean = false, func: SparkSession => Unit): Unit = {
    try {
      initializeSession()

      func(sparkSession)
    } finally {
      if (stop) {
        sparkSession.stop()
        System.clearProperty("spark.driver.port")
      }
    }
  }

  def initializeSession(): Unit = {
    sparkSession.sparkContext.setLogLevel("INFO")
  }
}
