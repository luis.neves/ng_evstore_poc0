package app

import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.SparkSession

object Env {

  // This method checks some properties and configuration values
  // to try to determine if Spark is running in an EMR environment.
  // It's fuzzy, but it's good enough for our purposes.
  def isEMR(spark: SparkSession): Boolean = {

    val configs = spark.sparkContext.getConf.getAll

    val emrMatches: Int = configs.map(conf => {
      StringUtils.countMatches(conf._1.concat(conf._2).toLowerCase, ".emr")
    }).sum.intValue()

    if (StringUtils.contains(System.getProperty("os.version"), "amzn") && (emrMatches > 5))
      true
    else
      false
  }

  def s3Scheme(spark: SparkSession): String = {

    if (isEMR(spark))
      "s3"
    else
      "s3a"
  }

}
