package app

import inet.ipaddr.{IPAddress, IPAddressString, IPAddressStringParameters}

import java.math.BigInteger

object InetUtils {

  private val ipAddrStrParams: IPAddressStringParameters = new IPAddressStringParameters.Builder().allow_inet_aton(false).toParams

  def ipToDecimal(ip_str: String): BigInteger = {
    val addr: IPAddress = (new IPAddressString(ip_str, ipAddrStrParams)).getAddress
    addr.getValue
  }

  def fullIp(ip_str: String): String = {
    val addr: IPAddress = (new IPAddressString(ip_str)).getAddress
    addr.toFullString
  }
}