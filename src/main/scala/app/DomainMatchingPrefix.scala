package app

import org.apache.spark.internal.Logging
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{SaveMode, SparkSession}

import java.time.LocalDate

object DomainMatchingPrefix extends Logging {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("domain_matching_suffix")
      .config("spark.hadoop.fs.s3a.path.style.access", "true")
      .getOrCreate()

    val fs = Env.s3Scheme(spark)

    val hostsSchema = StructType(Array(
      StructField("hostname", StringType, true)
    ))

    val edschema = StructType(Array(
      StructField("domain", StringType, true),
      StructField("val_from", DateType, true),
      StructField("val_until", DateType, true),
      StructField("entity_id", StringType, true),
      StructField("tags", StringType, true),
      StructField("xxx", StringType, true)
    ))

    val udPath = s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/unique_domains.txt.gz"

    spark
      .read
      .schema(hostsSchema)
      .csv(udPath)
      .withColumn("ev_date", current_date())
      .withColumn("rev_hostname", Udf.reverse_domain(col("hostname")))
      .createOrReplaceTempView("event_data")

    val dt = LocalDate.now;
    val rdPath = f"${fs}%s://com.bitsighttech.pipeline.entitymaps.production/all-time/${dt.getYear}%s/${dt.getMonthValue}%02d/${dt}%s_entities-resolved-domain-all-time.csv.gz"

    spark
      .read
      .schema(edschema)
      .csv(rdPath)
      .withColumn("rev_domain", Udf.reverse_domain(col("domain")))
      .withColumn("part_count", Udf.count_parts(col("domain")))
      .createOrReplaceTempView("resolved_domain")

    spark.time(spark.sql(
      """
WITH hosts_cte AS (
SELECT
  *
  , ROW_NUMBER() OVER (PARTITION BY ed.hostname ORDER BY rd.part_count DESC) AS ix
FROM
  event_data ed, resolved_domain rd
 WHERE
  ed.rev_hostname LIKE CONCAT(rd.rev_domain, '%') ESCAPE '$'
  AND ed.ev_date >= rd.val_from
  AND ed.ev_date <= rd.val_until
)
SELECT hostname, entity_id, domain FROM hosts_cte WHERE ix = 1
             """)
      .write
      .mode(SaveMode.Overwrite)
      .csv(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/matched_domains_suffix/"))

  }
}
