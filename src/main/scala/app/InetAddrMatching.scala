package app

import org.apache.commons.lang3.StringUtils
import org.apache.spark.internal.Logging
import org.apache.spark.sql.functions.{lit, _}
import org.apache.spark.sql.types.{StructField, _}
import org.apache.spark.sql.{SaveMode, SparkSession}

import java.time.LocalDate

object InetAddrMatching extends Logging {
  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("inet_matching")
      .config("spark.hadoop.fs.s3a.path.style.access", "true")
      .getOrCreate()

    val fs = Env.s3Scheme(spark)

    val ipSchema = StructType(Array(
      StructField("ev_date", DateType, false)
      , StructField("ip_str", StringType, false)
    ))

    // load the the "event ip" data
    // the data looks like this:
    //      2022-07-09,157.25.241.74
    //      2022-04-02,e515:f67c:2a59:eb12:4acc:4fc6:af05:645c
    //      2022-08-10,222.230.210.219

    spark
      .read
      .schema(ipSchema)
      .csv(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ev_ips.txt.gz")
      .withColumn("inet_addr", Udf.full_inet_addr(col("ip_str")))
      .withColumn("inet_version", Udf.inet_version(col("ip_str")))
      .withColumn("dec_inet_addr", Udf.ip_to_decimal(col("ip_str")))
      .withColumn("ip_seg1", Udf.seg1(col("inet_addr")))
      .withColumn("ip_seg2", Udf.seg2(col("inet_addr")))
      .withColumn("ev_year", year(col("ev_date")))
      .drop("ip_str")
      .orderBy("ev_date", "inet_addr")
      .write
      .partitionBy("inet_version")
      .mode(SaveMode.Overwrite)
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_ev_ips/")

    spark
      .read
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_ev_ips/")
      .createOrReplaceTempView("event_ip")

    val dateBoundaries = spark
      .sql("SELECT MIN(ev_date) AS min_date, MAX(ev_date) AS max_date FROM event_ip")
      .collectAsList()

    val minDate = dateBoundaries.get(0).getDate(0)
    val maxDate = dateBoundaries.get(0).getDate(1)

    val entitiesFilePath = s"${fs}://com.bitsighttech.pipeline.entitydata.production/ms_tables/prd1/LATEST/entities.csv.gz"

    // load the entities metadata file, we need this to translate the entity_guid -> entity_id
    spark.read
      .option("delimiter", ",")
      .option("header", "true")
      .option("multiLine", "true")
      .csv(entitiesFilePath)
      .select(col("id"), col("guid"))
      .write
      .mode(SaveMode.Overwrite)
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_aux_entity_info/")


    val dt = LocalDate.now;
    val riPath = f"${fs}%s://com.bitsighttech.pipeline.entitymaps.production/all-time/${dt.getYear}%s/${dt.getMonthValue}%02d/${dt}%s_entities-resolved-ips-all-time.csv.gz"

    val riSchema = StructType(Array(
      StructField("ip_info", StringType, true)
      , StructField("date_info", StringType, true)
      , StructField("entity_guid", StringType, true)
    ))

    // load and parse the "resolved-ips" file
    // filter out records that are not relevant to the current data
    // "adjust" the date boundaries to be inline with the current data
    spark
      .read
      .schema(riSchema)
      .csv(riPath)
      .withColumn("ip_range", Udf.parse_ip_range(col("ip_info")))
      .withColumn("date_range", Udf.parse_date_range(col("date_info")))
      .select(
        col("ip_range.begin").as("ip_begin")
        , col("ip_range.end").as("ip_end")
        , col("date_range.begin").as("date_begin")
        , col("date_range.end").as("date_end")
        , col("entity_guid")
      )
      .filter(col("date_end") > lit(minDate))
      .withColumn("date_begin", greatest(col("date_begin"), lit(minDate)))
      .withColumn("date_end", least(col("date_end"), lit(maxDate)))
      .write
      .mode(SaveMode.Overwrite)
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_aux_resolved_ips/")


    spark
      .read
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_aux_entity_info/")
      .createOrReplaceTempView("aux_entity_info")

    spark
      .read
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_aux_resolved_ips/")
      .createOrReplaceTempView("aux_resolved_ips")

    // swap the entity_guid for the entity_id
    // add columns that will be used for "bucketing"
    spark.sql(
      """
SELECT
  aux_entity_info.id AS entity_id
  , r_ips.ip_begin
  , r_ips.ip_end
  , r_ips.date_begin
  , r_ips.date_end
FROM
  aux_resolved_ips r_ips INNER JOIN aux_entity_info ON (r_ips.entity_guid = aux_entity_info.guid)
        """)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_aux_eid_resolved_ips/")


    spark
      .read
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_aux_eid_resolved_ips/")
      .withColumn("inet_version", Udf.inet_version(col("ip_begin")))
      .withColumn("dec_ip_begin", Udf.ip_to_decimal(col("ip_begin")))
      .withColumn("dec_ip_end", Udf.ip_to_decimal(col("ip_end")))
      .withColumn("ip_begin", Udf.full_inet_addr(col("ip_begin")))
      .withColumn("ip_end", Udf.full_inet_addr(col("ip_end")))
      .withColumn("year", explode(Udf.year_range(col("date_begin"), col("date_end"))))
      .withColumn("ip_seg1", explode(Udf.ip_segment_range(col("ip_begin"), col("ip_end"), lit(1))))
      .withColumn("ip_seg2", explode(Udf.ip_segment_range(col("ip_begin"), col("ip_end"), lit(2))))
      .write
      .partitionBy("inet_version")
      .mode(SaveMode.Overwrite)
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_resolved_ips/")


    spark
      .read
      .parquet(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/pq_resolved_ips/")
      .createOrReplaceTempView("resolved_ips")


    spark.time(spark.sql(
      """
SELECT
  event_ip.inet_addr
  , event_ip.ev_date
  , resolved_ips.entity_id
FROM
  event_ip, resolved_ips
WHERE
  event_ip.inet_version = resolved_ips.inet_version
  AND event_ip.ev_year = resolved_ips.year
  AND event_ip.ip_seg1 = resolved_ips.ip_seg1
  AND event_ip.ip_seg2 = resolved_ips.ip_seg2
  AND event_ip.ev_date >= resolved_ips.date_begin
  AND event_ip.ev_date <= resolved_ips.date_end
  AND event_ip.dec_inet_addr >= resolved_ips.dec_ip_begin
  AND event_ip.dec_inet_addr <= resolved_ips.dec_ip_end
        """
    )
      .write
      .mode(SaveMode.Overwrite)
      .csv(s"${fs}://com.bitsighttech.tmp.autodelete.ninetyday/iceberg_poc0/ip_mapping/matched_ips/")
    )

  }
}
