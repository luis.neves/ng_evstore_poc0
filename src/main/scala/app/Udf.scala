package app

import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.types.DataTypes

import java.time.LocalDate

object Udf {

  val reverse_domain = udf((domainName: String) => Option(domainName).map(s => reverseDomain(s)))
  val count_parts = udf((domainName: String) => Option(domainName).map(s => countParts(s)))
  val parse_ip_range = udf((data: String) => Option(data).map(s => doParseIpRange(s)))
  val parse_date_range = udf((data: String) => Option(data).map(s => doParseDateRange(s)))
  val seg1 = udf((data: String) => Option(data).map(s => doSeg(s, 1)))
  val seg2 = udf((data: String) => Option(data).map(s => doSeg(s, 2)))
  val ip_to_decimal = udf((data: String) => Option(data).map(s => InetUtils.ipToDecimal(s)))
  val inet_version = udf(
    (ip_str: String)
    =>
      Option(ip_str).map(s => if (StringUtils.contains(ip_str, ":")) "IPv6" else "IPv4")
  )
  val full_inet_addr = udf(
    (ip_str: String)
    =>
      Option(ip_str).map(s => InetUtils.fullIp(s))
  )
  val year_range = udf((begin: java.sql.Date, end: java.sql.Date) => (begin.toLocalDate.getYear to end.toLocalDate.getYear).map(_.toInt))
  val ip_segment_range = udf((begin: String, end: String, ix: Int) => {
    if (begin.contains(":")) {
      (Integer.parseInt(doSeg(begin, ix), 16) to Integer.parseInt(doSeg(end, ix), 16))
        .map(n => StringUtils.leftPad(Integer.toString(n, 16), 4, "0"))
    } else {
      (Integer.parseInt(doSeg(begin, ix)) to Integer.parseInt(doSeg(end, ix)))
        .map(n => StringUtils.leftPad(Integer.toString(n), 4, "0"))
    }
  })
  val generate_subddomains = udf((domainName: String) => Option(domainName).map(s => generateSubdomains(s).toArray))

  def reverseDomain(domainName: String): String = {
    val parts = StringUtils.split(domainName, ".").reverse
    parts.mkString("", ".", ".")
  }

  def countParts(domainName: String): Int = {
    StringUtils.countMatches(domainName, '.')
  }

  def generateSubdomains(domainName: String): IndexedSeq[(Int, String)] = {
    val parts = StringUtils.split(domainName, ".")
    for (i <- 0 to parts.length - 2) yield (parts.length - i, (parts.drop(i).mkString("", ".", ".")))
  }

  private def doParseIpRange(text: String): IpRange = {
    val ipr = StringUtils.split(text, "-")
    IpRange(ipr(0), ipr(1))
  }

  private def doParseDateRange(text: String): DateRange = {
    val dr = StringUtils.split(text, ":")
    DateRange(java.sql.Date.valueOf(LocalDate.parse(dr(0))), java.sql.Date.valueOf(LocalDate.parse(dr(1))))
  }

  private def doSeg(text: String, ix: Int): String = {
    StringUtils.split(text, ".:")
      .drop(ix - 1)
      .take(1)
      .map(p => StringUtils.leftPad(p, 4, "0"))
      .mkString
  }

  case class IpRange(begin: String, end: String)

  case class DateRange(begin: java.sql.Date, end: java.sql.Date)
}
