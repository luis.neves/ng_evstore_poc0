package app.shodan

import org.apache.spark.sql.types._

object SourceShodan {

  def getSchema(): StructType = {

    StructType(Array(
      StructField("_shodan", StructType(
        StructField("id", StringType) ::
          StructField("module", StringType) ::
          StructField("crawler", StringType) ::
          StructField("options", StructType(
            StructField("hostname", StringType) ::
              StructField("referrer", StringType) ::
              StructField("scan", StringType) :: Nil), true) ::
          StructField("ptr", BooleanType) :: Nil), true),
      StructField("timestamp", TimestampType, false),
      StructField("hash", IntegerType, false),
      StructField("asn", StringType, true),
      StructField("transport", StringType, false),
      StructField("info", StringType, true),
      StructField("ip_str", StringType, true),
      StructField("ipv6", StringType, true),
      StructField("port", IntegerType, false),
      StructField("version", IntegerType, true),
      StructField("isp", StringType, true),
      StructField("os", StringType, true),
      StructField("product", StringType, true),
      StructField("domains", ArrayType(StringType), false),
      StructField("hostnames", ArrayType(StringType), false),
      StructField("tags", ArrayType(StringType), true),
      StructField("ssl", StringType, true),
      StructField("cpe", ArrayType(StringType), true),
      StructField("cpe23", ArrayType(StringType), true),
      StructField("vulns", MapType(
        StringType,
        StructType(StructField("cvss", DoubleType) ::
          StructField("references", ArrayType(StringType)) ::
          StructField("summary", StringType) ::
          StructField("verified", BooleanType) :: Nil))
        , true),
      StructField("data", StringType, true),
      StructField("http", StructType(
          StructField("host", StringType) ::
            StructField("location", StringType) ::
            StructField("redirects", ArrayType(StructType(
              StructField("host", StringType) ::
              StructField("location", StringType) :: Nil))) :: Nil), true)
    ))
  }
}