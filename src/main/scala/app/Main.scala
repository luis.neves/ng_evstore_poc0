package app

import org.apache.spark.internal.Logging

object Main extends Logging {

  def main(args: Array[String]): Unit = {

    ComputeGlueCatalog.initializeSession()

    ComputeGlueCatalog.withSpark(false, spark => {

      //val pathToRawShodanFiles = "s3://com.bitsighttech.collection.production/shodan/2023/03/{01,02,03,04,05,06,07}/*.json.gz"


      val shodanTableName = "ev_shodan"
      AggShodan.createEvShodanTable(spark, shodanTableName)

      for (a <- 1 to 7) {
        val pathToRawShodanFiles = s"s3://com.bitsighttech.collection.production/shodan/2023/03/0$a/*.json.gz"
        AggShodan.loadEvShodanTable(spark, pathToRawShodanFiles, shodanTableName)
      }


    })
  }
}