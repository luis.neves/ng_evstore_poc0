package com.bitsighttech.minimal;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The TagValue class represents the value of a BitSight Tag.
 *
 */
public final class TagValue  {

  /**
   * Lazy Value.
   */
  public interface LazyValue {

    /**
     * Resolve the value.
     *
     * @return the {@link TagValue}.
     */
    TagValue resolve();

  }

  /** The value. */
  protected Object value;

  private static final TagValue NIL = new TagValue();

  /** For tests. */
  boolean numberFormatException;
  
  /**
   * Create a "null" value.
   */
  public TagValue() {
    super();
    this.value = null;
  }

  /**
   * Create an {@link Integer} value.
   *
   * @param val
   *          the int value.
   */
  public TagValue(final int val) {
    super();
    this.value = Integer.valueOf(val);
  }

  /**
   * Create an {@link Integer} value by providing an {@link Integer}.
   *
   * @param val
   *          the {@link Integer} value.
   */
  public TagValue(final Integer val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link Long} value.
   *
   * @param val
   *          the long value.
   */
  public TagValue(final long val) {
    super();
    this.value = Long.valueOf(val);
  }

  /**
   * Create a {@link Long} value by providing a {@link Long}.
   *
   * @param val
   *          the {@link Long} value.
   */
  public TagValue(final Long val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link Float} value.
   *
   * @param val
   *          the float value.
   */
  public TagValue(final float val) {
    super();
    this.value = Float.valueOf(val);
  }

  /**
   * Create a {@link Float} value by providing a {@link Float}.
   *
   * @param val
   *          the {@link Float} value.
   */
  public TagValue(final Float val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link Double} value.
   *
   * @param val
   *          the double value.
   */
  public TagValue(final double val) {
    super();
    this.value = Double.valueOf(val);
  }

  /**
   * Create a {@link Double} value by providing a {@link Double}.
   *
   * @param val
   *          the {@link Double} value.
   */
  public TagValue(final Double val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link Boolean} value.
   *
   * @param val
   *          the boolean value.
   */
  public TagValue(final boolean val) {
    super();
    this.value = Boolean.valueOf(val);
  }

  /**
   * Create a {@link Boolean} value by providing a {@link Boolean}.
   *
   * @param val
   *          the {@link Boolean} value.
   */
  public TagValue(final Boolean val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link String} value.
   *
   * @param val
   *          the {@link String} value.
   */
  public TagValue(final String val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link CountryRegionCodePair} value.
   *
   * @param val
   *          the {@link CountryRegionCodePair} value.
   */
  public TagValue(final CountryRegionCodePair val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link String} value.
   *
   * @param val
   *          the {@link String} value.
   * @return the {@link String} {@link TagValue}.
   */
  public static TagValue fromString(final String val) {
    return new TagValue(val);
  }

  /**
   * Create an {@link EvidenceKey} value.
   *
   * @param val
   *          the {@link EvidenceKey} value.
   */
  public TagValue(final EvidenceKey val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link LazyValue} value.
   *
   * @param val
   *          the {@link LazyValue} value.
   */
  public TagValue(final LazyValue val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link List} of {@link TagValue} value.
   *
   * @param val
   *          the {@link List} of {@link TagValue}.
   */
  public TagValue(final List<TagValue> val) {
    super();
    this.value = val;
  }

  /**
   * Create a {@link List} of {@link TagValue} value.
   *
   * @param val
   *          the {@link List} of {@link TagValue}.
   * @return the {@link List} of {@link TagValue} {@link TagValue}.
   */
  public static TagValue fromList(final List<TagValue> val) {
    return new TagValue(val);
  }

  /**
   * Create a {@link List} of {@link String} value.
   *
   * <p>
   * {@link TagValue#TagValue(Object)} will convert the {@link Object} into {@link String}.
   * </p>
   *
   * @param val
   *          the {@link List} of {@link Object}.
   * @see TagValue#TagValue(Object)
   */
  public TagValue(final Collection<?> val) {
    this((Object) val);
  }

  /**
   * Create a {@link List} of {@link String} value.
   *
   * <p>
   * {@link TagValue#TagValue(Object)} will convert the {@link Object} into {@link String}.
   * </p>
   *
   * @param val
   *          the {@link List} of {@link Object}.
   * @return the {@link List} of {@link Object} {@link TagValue}.
   * @see TagValue#TagValue(Object)
   */
  public static TagValue fromCollection(final Collection<Object> val) {
    return new TagValue(val);
  }

  /**
   * Create an {@link Enum} value.
   *
   * @param <T>
   *          the {@link Enum} type.
   * @param val
   *          the {@link Enum} value
   */
  public <T extends Enum<T>> TagValue(final Enum<T> val) {
    this.value = val;
  }

  /**
   * Create an object value. This method is private to make sure that we aren't passing in
   * strange object types.
   *
   * <p>
   * {@link Collection} of {@link Object}, not {@link List}, will be converted into {@link List}
   * with items converted into {@link String}.
   * </p>
   *
   * @param val
   *          the object value.
   */
  private TagValue(final Object val) {
    super();
    if (val != null && val instanceof Collection<?> && !(val instanceof List<?>)) {
      final List<String> list = new ArrayList<>();
      for (final Object obj : (Collection<?>) val) {
        list.add(obj.toString());
      }
      this.value = list;
    } else {
      this.value = val;
    }
  }

  /**
   * Warning: If you are using this method to do something simple, then you are doing the
   * wrong thing. This only works under specific circumstances. Even if it appears to work,
   * it may still not work.
   *
   * <p>This allows an arbitrary object to be inserted into a TagValue -- but there is no
   * guarantee that it will serialize correctly.
   * </p>
   *
   * @param object The object
   * @return the new TagValue
   */
  public static TagValue fromObjectWithoutHadoopSerialization(final Object object) {
    return new TagValue(object);
  }

  /**
   * Get the raw underlying object.
   *
   * @return the value.
   */
  public Object getValue() {
    return this.value;
  }

  private void resolveValue() {
    if (this.value instanceof LazyValue) {
      final TagValue resolved = ((LazyValue) this.value).resolve();
      this.value = resolved.getResolvedValue();
    }
  }

  /**
   * Get the value, after {@link LazyValue#resolve()} if needed.
   *
   * @return the value.
   */
  public Object getResolvedValue() {
    this.resolveValue();
    return this.value;
  }

  /**
   * Get a {@link Boolean}, possibly converting from other types.
   *
   * @return the {@link Boolean} value.
   */
  public Boolean getBoolean() {
    this.resolveValue();
    if (Boolean.class.isInstance(this.value)) {
      return (Boolean) this.value;
    }
    return Boolean.valueOf(this.getString());
  }

  /**
   * Get an {@link Integer}, or defValue if no integer can be found.
   *
   * @param defValue
   *          the default value.
   * @return the {@link Integer} value, or the default.
   */
  public Integer getInt(final Integer defValue) {
    this.resolveValue();
    if (Integer.class.isInstance(this.value)) {
      return (Integer) this.value;
    }
    try {
      final String sValue = this.getString();
      return sValue.isEmpty() ? defValue : Integer.valueOf(sValue);
    } catch (final NumberFormatException e) {
      this.numberFormatException = true;
      return defValue;
    }
  }

  private static final Integer ZEROI = Integer.valueOf(0);

  /**
   * Get an {@link Integer}, or 0 if no integer can be found.
   *
   * @return the {@link Integer} value, or 0.
   */
  public Integer getInt() {
    return this.getInt(ZEROI);
  }

  /**
   * Get an {@link Long}, converting if necessary, or defValue if no long can be found.
   *
   * @param defValue
   *          the default value.
   * @return the {@link Long} value, or the default.
   */
  public Long getLong(final Long defValue) {
    this.resolveValue();
    if (Long.class.isInstance(this.value)) {
      return (Long) this.value;
    }
    if (Integer.class.isInstance(this.value)) {
      return Long.valueOf(((Integer) this.value).longValue());
    }
    try {
      final String sValue = this.getString();
      return sValue.isEmpty() ? defValue : Long.valueOf(sValue);
    } catch (final NumberFormatException e) {
      this.numberFormatException = true;
      return defValue;
    }
  }

  private static final Long ZEROL = Long.valueOf(0);

  /**
   * Get a {@link Long}, converting if necessary, or 0 if no long can be found.
   *
   * @return the {@link Long} value, or 0.
   */
  public Long getLong() {
    return this.getLong(ZEROL);
  }

  /**
   * Get a {@link Float}, converting if necessary, or defValue if no float can be found.
   *
   * @param defValue
   *          the default value.
   * @return the {@link Float} value, or the default.
   */
  public Float getFloat(final Float defValue) {
    this.resolveValue();
    if (Float.class.isInstance(this.value)) {
      return (Float) this.value;
    }
    if (this.value instanceof Number) {
      return Float.valueOf(((Number) this.value).floatValue());
    }
    try {
      final String sValue = this.getString();
      return sValue.isEmpty() ? defValue : Float.valueOf(sValue);
    } catch (final NumberFormatException e) {
      this.numberFormatException = true;
      return defValue;
    }
  }

  private static final Float ZEROF = Float.valueOf(0);

  /**
   * Get a {@link Float}, converting if necessary, or 0.0f if no float can be found.
   *
   * @return the {@link Float} value, or 0.0f.
   */
  public Float getFloat() {
    return this.getFloat(ZEROF);
  }

  /**
   * Get a {@link Double}, converting if necessary, or defValue if no double can be found.
   *
   * @param defValue
   *          the default value.
   * @return the {@link Double} value, or the default.
   */
  public Double getDouble(final Double defValue) {
    this.resolveValue();
    if (Double.class.isInstance(this.value)) {
      return (Double) this.value;
    }
    if (this.value instanceof Number) {
      return Double.valueOf(((Number) this.value).doubleValue());
    }
    try {
      final String sValue = this.getString();
      return sValue.isEmpty() ? defValue : Double.valueOf(sValue);
    } catch (final NumberFormatException e) {
      this.numberFormatException = true;
      return defValue;
    }
  }

  private static final Double ZEROD = Double.valueOf(0);

  /**
   * Get a {@link Double}, converting if necessary, or 0.0d if no double can be found.
   *
   * @return the {@link Double} value, or 0.0d;
   */
  public Double getDouble() {
    return this.getDouble(ZEROD);
  }

  /**
   * Get a {@link List} of {@link TagValue}, or defValue of no list can be found.
   *
   * @param defaultValue
   *          the default value.
   * @return the {@link List} of {@link TagValue} value, or the default.
   */
  @SuppressWarnings("unchecked")
  public List<TagValue> getList(final List<TagValue> defaultValue) {
    this.resolveValue();
    if (List.class.isInstance(this.value)) {
      return (List<TagValue>) this.value;
    }
    return defaultValue;
  }

  /**
   * Get a {@link List} of {@link TagValue}, or an empty list of no list can be found.
   *
   * @return the {@link List} of {@link TagValue} value, or the default.
   */
  public List<TagValue> getList() {
    return this.getList(new ArrayList<>());
  }

  /**
   * Get a {@link String}, converting if necessary, or defValue if the value is null.
   *
   * @param defValue
   *          The default value
   * @return the {@link String} value, or the default.
   */
  public String getString(final String defValue) {
    this.resolveValue();
    if (this.value != null) {
      return this.value.toString();
    }
    return defValue;
  }

  /**
   * Get a {@link String}, converting if necessary, or return an empty string if the value is null.
   *
   * @return the {@link String} value, or the empty string.
   */
  public String getString() {
    return this.getString("");
  }

  /**
   * Gets an {@link EvidenceKey} if possible.
   *
   * @return the {@link EvidenceKey} value.
   */
  public EvidenceKey getEvidenceKey() {
    this.resolveValue();
    if (this.value == null || this.value instanceof EvidenceKey) {
      return (EvidenceKey) this.value;
    }
    return EvidenceKey.valueOf(this.getString());
  }

  /**
   * Gets an {@link CountryRegionCodePair} if possible, otherwise Null.
   *
   * @return the {@link CountryRegionCodePair} value.
   */
  public CountryRegionCodePair getCountryRegionCodePair() {
    this.resolveValue();
    if (this.value == null || this.value instanceof CountryRegionCodePair) {
      return (CountryRegionCodePair) this.value;
    }
    return null;
  }

  /**
   * Get an {@link Enum} value, converting if necessary, or defValue if the value is not valid.
   *
   * @param <T>
   *          the {@link Enum} type.
   * @param enumType
   *          the {@link Enum} class.
   * @param defValue
   *          the default value.
   * @return the {@link Enum} value, or the default.
   */
  public <T extends Enum<T>> T getEnum(final Class<T> enumType, final T defValue) {
    if (this.value == null) {
      return defValue;
    }
    try {
      final String sValue = this.getString();
      return sValue.isEmpty() ? defValue : Enum.valueOf(enumType, sValue);
    } catch (final IllegalArgumentException e) {
      return defValue;
    }
  }
  
 

  /**
   * Examine the value to see if it is null.
   *
   * @return true if null.
   */
  public boolean isNull() {
    return this.value == null;
  }

  @Override
  public String toString() {
    return this.getString();
  }


  @Override
  public int hashCode() {
    if (this.value == null) {
      return 0;
    }
    if (Enum.class.isInstance(this.value)) {
      return this.getString().hashCode();
    }
    return this.value.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }

    final TagValue other = (TagValue) obj;
    if (this.value == other.value) {
      return true;
    }
    if (this.value == null) {
      return false;
    }
    if (other.value == null) {
      return false;
    }
    this.resolveValue();
    other.resolveValue();
    if (this.value.getClass() != other.value.getClass()) {
      // Need to deal with the Enum/String problem
      Object vmine = this.value;
      Object vother = other.value;
      if (vmine instanceof Enum) {
        vmine = this.getString();
      }
      if (vother instanceof Enum) {
        vother = other.getString();
      }
      return vmine.equals(vother);
    }
    return this.value.equals(other.value);
  }



  /**
   * Return the value, or nil {@link TagValue} if null.
   *
   * @param value
   *          the value.
   * @return the value, or a nil {@link TagValue} if null.
   */
  public static TagValue ensure(final TagValue value) {
    return value == null ? NIL : value;
  }

  /**
   * Get a {@link TagValue} for the given value.
   *
   * @param value
   *          the object value.
   * @return the corresponding {@link TagValue}.
   */
  @SuppressWarnings("unchecked")
  public static TagValue fromObject(final Object value) {
    if (Integer.class.isInstance(value)) {
      return new TagValue((Integer) value);
    }
    if (Long.class.isInstance(value)) {
      return new TagValue((Long) value);
    }
    if (Double.class.isInstance(value)) {
      return new TagValue((Double) value);
    }
    if (Float.class.isInstance(value)) {
      return new TagValue((Float) value);
    }
    if (Boolean.class.isInstance(value)) {
      return new TagValue((Boolean) value);
    }
    if (String.class.isInstance(value)) {
      return new TagValue((String) value);
    }
    if (EvidenceKey.class.isInstance(value)) {
      return new TagValue((EvidenceKey) value);
    }
    if (LazyValue.class.isInstance(value)) {
      return new TagValue((LazyValue) value);
    }
    if (TagValue.class.isInstance(value)) {
      return new TagValue(((TagValue) value).getValue());
    }
    if (List.class.isInstance(value)) {
      final List<?> list = (List<?>) value;
      if (list.isEmpty()) {
        return new TagValue(new ArrayList<TagValue>());
      }
      if (!(list.get(0) instanceof TagValue)) {
        throw new RuntimeException(
            String.format("The list contains an object of type %s ",
                list.get(0).getClass().getSimpleName()));
      }
      return new TagValue((List<TagValue>) value);
    }
    throw new RuntimeException(
        String.format("Cannot create a new TagValue from an objects of type %s ",
            value.getClass().getSimpleName()));
  }

}
