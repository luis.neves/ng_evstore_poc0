package com.bitsighttech.minimal;

import java.util.Arrays;

public class ByteArray {
  byte[] data;

  public ByteArray() {}

  public ByteArray(byte[] src) {
    data = src;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    return Arrays.equals(data, ((ByteArray) obj).data);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(data) & 0x7fffffff;
  }

  @Override
  public String toString() {
    // TODO Auto-generated method stub
    return new String(data);
  }



  public byte[] getData() {
    return data;
  }

  public int getDataLength() {
    return data.length;
  }

  public static int hashCodeFor(byte[] bytes) {
    return Arrays.hashCode(bytes) & 0x7fffffff;
  }

}
