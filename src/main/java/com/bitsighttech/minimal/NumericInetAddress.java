package com.bitsighttech.minimal;

import com.google.common.net.InetAddresses;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Numeric {@link InetAddress}.
 */
public final class NumericInetAddress {

  private static final InetAddress LOCALHOST = NumericInetAddress.forIp("127.0.0.1");

  /**
   * Get a numeric {@link InetAddress} for 127.0.0.1.
   *
   * <p>
   * Note: won't do a reverse lookup to find out the hostname of the machine!
   * </p>
   *
   * @return a numeric InetAddress for 127.0.0.1.
   */
  public static InetAddress for127() {
    return NumericInetAddress.LOCALHOST;
  }

  /**
   * Get a numeric {@link InetAddress} for the given IP.
   *
   * <p>
   * No hostname lookup, nor reverse IP lookup will be performed.
   * </p>
   * <p>
   * Note: given null, returns null. This is different from
   * {@link InetAddress#getByName(String)}!
   * </p>
   *
   * @param ip
   *          the specified ip, or {@code null}.
   * @return an IP address for the given host name.
   * @deprecated use #forIp(String)
   */
  @Deprecated
  public static InetAddress forName(final String ip) {
    return NumericInetAddress.forIp(ip);
  }

  /**
   * Get a numeric {@link InetAddress} for the given IP.
   *
   * <p>
   * No hostname lookup, nor reverse IP lookup will be performed.
   * </p>
   * <p>
   * Note: given null, returns null. This is different from
   * {@link InetAddress#getByName(String)}!
   * </p>
   *
   * @param ip
   *          the specified ip, or {@code null}.
   * @return an IP address for the given host name.
   * @see InetAddresses#forString(String)
   * @see InetAddress#getByName(String)
   */
  public static InetAddress forIp(final String ip) {
    NumericInetAddress.optimized = false;

    // Note: this is different from InetAddress.getByName(null)!
    if (ip == null) {
      return null;
    }

    // Lets try a fast approach for IPv4 addresses
    final int len = ip.length();
    if (len >= 7 && ip.indexOf('.') != -1) {
      boolean ok = true;
      final byte[] addr = new byte[4];
      for (int i = 0, start = 0; i < 4; i++) {
        final int pos = i == 3 ? len : ip.indexOf('.', start);
        if (pos == -1) {
          ok = false;
          break;
        }
        final int v = NumericInetAddress.parseIntSubtring(ip, start, pos);
        if (v < 0 || v > 255) {
          ok = false;
          break;
        }
        addr[i] = (byte) v;
        start = pos + 1;
      }
      if (ok) {
        try {
          return InetAddress.getByAddress(addr);
        } catch (final UnknownHostException e) {
          ok = false;
        } finally {
          /* for tests. */
          NumericInetAddress.optimized = true;
        }
      }
    }

    return InetAddresses.forString(ip);
  }

  /*
   * Beauties of JVM. It may look contra-intuitive to have a separate function to parse
   * the number, but because the method (the class) is final, when HotSpot does see this
   * method being called too often, it will compile it, and then because the result is
   * small enough, it will inline the code directly into the block above, making it
   * faster than if this cycle were up there.
   */
  private static int parseIntSubtring(final CharSequence string, final int begin, final int end) {
    int ret = 0;
    for (int i = begin; i < end; i++) {
      final char c = string.charAt(i);
      if (c < '0' || c > '9') {
        return -1;
      }
      ret = ret * 10 + c - '0';
    }
    return ret;
  }

  /** for tests. */
  static boolean optimized;

  private NumericInetAddress() {
    super();
    throw new UnsupportedOperationException();
  }

}
