package com.bitsighttech.minimal;

public interface HashCodeForTreeSet {
  public int hashCodeForTreeSet();
}
