package com.bitsighttech.minimal;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * Serializable helper to store a Pair of country and region codes strings.
 */
public class CountryRegionCodePair
    implements Comparable<CountryRegionCodePair>, Serializable {
  private static final long serialVersionUID = 10102L;

  private String countryCode;
  private String regionCode;

  public CountryRegionCodePair() {}

  /** Constructor. */
  public CountryRegionCodePair(String countryCode, String regionCode) {
    this.countryCode = countryCode;
    this.setRegionCode(regionCode);
  }

  @Override
  public int compareTo(CountryRegionCodePair rhs) {
    final CompareToBuilder ctb = new CompareToBuilder();

    ctb.append(this.countryCode, rhs.countryCode);
    ctb.append(this.regionCode, rhs.regionCode);


    return ctb.toComparison();
  }

  public String getCountryCode() {
    return this.countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getRegionCode() {
    return this.regionCode;
  }

  /**
   * Set the internal region code, if the provided code is empty we set the internal value
   * to null.
   * 
   * @param regionCode the region code.
   */
  public void setRegionCode(String regionCode) {
    if (StringUtils.isEmpty(regionCode)) {
      this.regionCode = null;
    } else {
      this.regionCode = regionCode;
    }
  }

  @Override
  public String toString() {
    if (this.regionCode == null) {
      return this.countryCode;
    }
    return String.format("%s-%s", this.countryCode, this.regionCode);
  }

  @Override
  public int hashCode() {
    final HashCodeBuilder hb = new HashCodeBuilder()
        .append(this.countryCode)
        .append(this.regionCode);

    return hb.toHashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }
    if (obj == this) {
      return true;
    }

    final CountryRegionCodePair rhs = (CountryRegionCodePair) obj;

    final EqualsBuilder eb = new EqualsBuilder()
        .append(this.countryCode, rhs.countryCode)
        .append(this.regionCode, rhs.regionCode);

    return eb.isEquals();
  }
}
