package com.bitsighttech.minimal;

import com.google.common.net.InetAddresses;

import org.apache.avro.reflect.AvroIgnore;
import org.apache.avro.reflect.AvroName;
import org.apache.avro.reflect.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.IDN;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * An EvidenceKey holds the key for some particular piece of evidence. This can be an IP
 * address, a Domain name, an ASN number, an Entity number, etc. It is intended that this
 * list grow over time.
 *
 */

public class EvidenceKey implements HashCodeForTreeSet, Comparable<EvidenceKey> {

  private static final Pattern DOMAIN_PATTERN = Pattern.compile("\\.");

  /**
   * Inner class.
   */
  public static class EvidenceKeyException extends RuntimeException {

    /**
     * class serial version id.
     */
    private static final long serialVersionUID = -8494395302730820328L;

    public EvidenceKeyException(String string) {
      super(string);
    }
  }

  /**
   * The KeyType enumeration defines all the possible types for an EvidenceKey. The type
   * of a particular object is returned by the getType method. The UNDEFINED KeyType
   * should be first so that it sorts before the other types.
   * 
   */
  public enum KeyType {
    UNDEFINED, INET_ADDRESS, AS_NUMBER, BOGON_LIST, ENTITY_ID, DOMAIN_NAME, INDUSTRY_ID,
    INET_ADDRESS_WITH_PORT, CERTIFICATE_ID, DOMAIN_NAME_WITH_PORT, STRING
  }

  @Nullable
  private KeyType type;
  @AvroIgnore
  private InetAddress address;
  @AvroName("address")
  @Nullable
  // CHECKSTYLE:OFF
  private ByteBuffer _address; // for avro schema generation
  // CHECKSTYLE:ON
  @Nullable
  protected String domain; // Stored reversed -- e.g. info.pskreporter.www
  @Nullable
  private int integer;
  @Nullable
  private long longInteger;

  private static final EvidenceKey nullEvidenceKey = new EvidenceKey();
  private static Map<Integer, KeyType> charToKeyType = new HashMap<Integer, KeyType>();

  static {
    charToKeyType.put((int) 'U', KeyType.UNDEFINED);
    charToKeyType.put((int) 'I', KeyType.INET_ADDRESS);
    charToKeyType.put((int) 'A', KeyType.AS_NUMBER);
    charToKeyType.put((int) 'B', KeyType.BOGON_LIST);
    charToKeyType.put((int) 'E', KeyType.ENTITY_ID);
    charToKeyType.put((int) 'N', KeyType.INDUSTRY_ID);
    charToKeyType.put((int) 'D', KeyType.DOMAIN_NAME);
    charToKeyType.put((int) 'P', KeyType.INET_ADDRESS_WITH_PORT);
    charToKeyType.put((int) 'C', KeyType.CERTIFICATE_ID);
    charToKeyType.put((int) 'p', KeyType.DOMAIN_NAME_WITH_PORT);
    charToKeyType.put((int) 'S', KeyType.STRING);
  }

  /**
   * Constructor used primarily for the hadoop Writable methods.
   */
  public EvidenceKey() {
    type = KeyType.UNDEFINED;
  }

  /**
   * Create an EvidenceKey for an inet address.
   *
   * @param address inet address
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forAddress(InetAddress address) {
    final EvidenceKey result = new EvidenceKey();
    result.type = KeyType.INET_ADDRESS;
    result.address = address;

    return result;
  }

  /**
   * Create an EvidenceKey for an inet address.
   *
   * @param address address
   * @param port port
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forAddress(InetAddress address, int port) {
    final EvidenceKey result = new EvidenceKey();
    result.type = KeyType.INET_ADDRESS_WITH_PORT;
    result.address = address;
    result.integer = port;
    return result;
  }

  /**
   * Create an EvidenceKey for a domain name.
   *
   * @param domain domain
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forDomain(String domain) {
    final EvidenceKey result = new EvidenceKey();
    result.type = KeyType.DOMAIN_NAME;
    while (domain.endsWith(".")) {
      domain = domain.substring(0, domain.length() - 1);
    }

    try {
      domain = IDN.toASCII(domain.toLowerCase());
    } catch (final IllegalArgumentException iaex) {
      // Fairly common. Just ignore these.
    } catch (final RuntimeException rtex) {
      System.err.printf("IDN.toASCII failed on:%n %s -> %s", domain.toLowerCase(), rtex
          .getMessage());
    }

    result.domain = reverseDomainName(domain).toLowerCase();

    return result;
  }

  /**
   * Create an EvidenceKey for a domain name.
   *
   * @param domain domain
   * @param address the associated IP address
   * @param port the port number
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forDomain(String domain, InetAddress address, int port) {
    System.out.println("EvidenceKey.domain: " +  domain);
    System.out.println("EvidenceKey.address: " +  address);
    System.out.println("EvidenceKey.port: " +  port);
    final EvidenceKey result = new EvidenceKey();
    result.type = KeyType.DOMAIN_NAME_WITH_PORT;
    while (domain.endsWith(".")) {
      domain = domain.substring(0, domain.length() - 1);
    }

    try {
      domain = IDN.toASCII(domain.toLowerCase());
    } catch (final IllegalArgumentException error) {
      // Not much we can do here
    }

    result.domain = reverseDomainName(domain).toLowerCase();
    result.integer = port;
    result.address = address;

    System.out.println("EvidenceKey.key: " +  result);

    return result;
  }

  public static EvidenceKey forDomain(String domain, EvidenceKey evidenceKey) {
    return forDomain(domain, evidenceKey.getAddress(), evidenceKey.getPort());
  }

  /**
   * Create an EvidenceKey for an AS Number.
   *
   * @param asn AS Number
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forAsNumber(int asn) {
    final EvidenceKey result = new EvidenceKey();

    result.type = KeyType.AS_NUMBER;
    result.integer = asn;

    return result;
  }

  /**
   * Create an EvidenceKey for an entityid.
   *
   * @param entityid entityid
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forEntityId(int entityid) {
    final EvidenceKey result = new EvidenceKey();

    result.type = KeyType.ENTITY_ID;
    result.integer = entityid;

    return result;
  }

  /**
   * Create an EvidenceKey for an industryid.
   *
   * @param industryid industryid
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forIndustryId(int industryid) {
    final EvidenceKey result = new EvidenceKey();

    result.type = KeyType.INDUSTRY_ID;
    result.integer = industryid;

    return result;
  }

  /**
   * Creates an EvidenceKey using a certificate identifier.
   *
   * @return An EvidenceKey object
   */

  public static EvidenceKey forCertificateId(long certId) {
    EvidenceKey result = new EvidenceKey();
    result.type = KeyType.CERTIFICATE_ID;
    result.longInteger = certId;

    return result;
  }

  /**
   * Creates an EvidenceKey for the bogon list.
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forBogonList() {
    EvidenceKey result = new EvidenceKey();
    result.type = KeyType.BOGON_LIST;

    return result;
  }

  /**
   * Creates an EvidenceKey for a generic string.
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey forString(String string) {
    EvidenceKey result = new EvidenceKey();

    result.type = KeyType.STRING;
    result.domain = string;

    return result;
  }

  /**
   * Return the domain for STRING type of EvidenceKey.
   */
  public String getString() {
    if (type == KeyType.STRING) {
      return this.domain;
    }
    throw new EvidenceKeyException(
        "getString called not on STRING type EvidenceKey! Type: " + type.name());
  }

  /**
   * Return the object type.
   *
   * @return the type
   */
  public KeyType getType() {
    return type;
  }

  /**
   * Method to return an empty evidence key.
   *
   * @return An EvidenceKey object
   */
  public static final EvidenceKey getNullEvidenceKey() {
    return nullEvidenceKey;
  }

  /**
   * Check to see if the evidence key has an associated IP address.
   *
   * @return true if this evidence key has an associated address
   */
  public boolean hasAddress() {
    if (type == KeyType.INET_ADDRESS || type == KeyType.INET_ADDRESS_WITH_PORT
        || type == KeyType.DOMAIN_NAME_WITH_PORT) {
      return address != null;
    }
    return false;
  }

  /**
   * Check to see if the evidence key is an IP or an IP with port.
   *
   * @return true if the evidence key is an address
   */
  public boolean isAddress() {
    return (this.type == KeyType.INET_ADDRESS || this.type == KeyType.INET_ADDRESS_WITH_PORT);
  }

  /**
   * Check to see if the evidence key has an associated port number.
   *
   * @return true if this evidence key has an associated port number
   */
  public boolean hasPort() {
    if (type == KeyType.DOMAIN_NAME_WITH_PORT || type == KeyType.INET_ADDRESS_WITH_PORT) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Check to see if the evidence key has an associated domain name.
   *
   * @return true if this evidence key has an associated domain name
   */
  public boolean hasDomain() {
    if (type == KeyType.DOMAIN_NAME_WITH_PORT || type == KeyType.DOMAIN_NAME) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Method to return the address in the object.
   *
   * @return the address
   */
  public InetAddress getAddress() {
    if (type == KeyType.INET_ADDRESS || type == KeyType.INET_ADDRESS_WITH_PORT
        || type == KeyType.DOMAIN_NAME_WITH_PORT) {
      return address;
    }
    throw new EvidenceKeyException("getAddress called on EvidenceKey of type " + type.name());
  }

  /**
   * Method to get the port.
   *
   * @return the port
   */
  public int getPort() {
    if (type == KeyType.INET_ADDRESS_WITH_PORT || type == KeyType.DOMAIN_NAME_WITH_PORT) {
      return integer;
    }
    throw new EvidenceKeyException("getPort called on EvidenceKey of type " + type.name());
  }

  /**
   * Method to get object domain info.
   *
   * @return the domain
   */
  public String getDomain() {
    return reverseDomainName(getRawDomain());
  }

  /**
   * Accessor to get the raw domain name.
   *
   * @return the raw domain name
   */
  public String getRawDomain() {
    if (type == KeyType.DOMAIN_NAME || type == KeyType.DOMAIN_NAME_WITH_PORT) {
      return domain;
    }
    throw new EvidenceKeyException("getDomain called on EvidenceKey of type " + type.name());
  }

  /**
   * Method to get domain components.
   *
   * @return the non-reversed components of the domain name converted to lower case
   */
  public String[] getDomainComponents() {
    String domainName = getRawDomain();
    return DOMAIN_PATTERN.split(domainName.toLowerCase());
  }

  /**
   * Get the domain in Unicode. Throws an exception if the EvidenceKey is of the wrong
   * type.
   *
   * @return the domain in presentation form
   */
  public String getPresentationDomain() {
    if (type != KeyType.DOMAIN_NAME && type != KeyType.DOMAIN_NAME_WITH_PORT) {
      throw new EvidenceKeyException("getPresentationDomain called on EvidenceKey of type "
          + type.name());
    }

    return IDN.toUnicode(reverseDomainName(domain));
  }

  /**
   * Method to return ASN number.
   *
   * @return the AS Number
   */
  public int getAsNumber() {
    if (type == KeyType.AS_NUMBER) {
      return integer;
    }
    throw new EvidenceKeyException("getAsNumber called on EvidenceKey of type " + type.name());
  }

  /**
   * Method to return EntityId.
   *
   * @return the EntityId
   */
  public int getEntityId() {
    if (type == KeyType.ENTITY_ID) {
      return integer;
    }
    throw new EvidenceKeyException("getEntityId called on EvidenceKey of type " + type.name());
  }

  /**
   * Method to get certificate id.
   *
   * @return the certificateId if applicable
   */
  public long getCertificateId() {
    if (type == KeyType.CERTIFICATE_ID) {
      return longInteger;
    }
    throw new EvidenceKeyException("getCertificateId called on EvidenceKey of type " + type.name());
  }

  /**
   * Method to get Industry id.
   *
   * @return the IndustyId
   */
  public int getIndustryId() {
    if (type != KeyType.INDUSTRY_ID) {
      throw new EvidenceKeyException("getIndustryId called on EvidenceKey of type " + type.name());
    }

    return integer;
  }

  /**
   * Gets an evidence key that does not include the domain name if there is also an
   * address. This is a short term method that is to be used while we are not handling
   * domain scanning properly.
   *
   * @return the cleaned EvidenceKey
   */
  public EvidenceKey removeDomainName() {
    if (getType() == KeyType.DOMAIN_NAME_WITH_PORT) {
      return EvidenceKey.forAddress(getAddress(), getPort());
    }

    return this;
  }

  /**
   * Gets an evidence key that does not include the address if there is also a domain.
   *
   * @return the cleaned EvidenceKey
   */
  public EvidenceKey removeAddress() {
    if (getType() == KeyType.DOMAIN_NAME_WITH_PORT && hasAddress()) {
      return EvidenceKey.forDomain(getDomain(), null, getPort());
    }

    return this;
  }

  /**
   * Method to reverse domain name.
   *
   * @param domain domain name
   *
   * @return An inverted domain name
   */
  public static String reverseDomainName(String domain) {
    int lastDot = domain.length();
    final StringBuilder sb = new StringBuilder(lastDot);
    final char[] chars = domain.toCharArray();

    for (int i = lastDot - 1; i >= 0; i--) {
      final char c = chars[i];
      if (c < 47) { // speedup for normal character
        if (c == 46) { // . (dot)
          sb.append(chars, i + 1, lastDot - i - 1);
          sb.append('.');
          lastDot = i;
        } else if (c == 0) { // null character
          throw new EvidenceKeyException(
              "Domain names cannot contain nulls: " + domain + " at offset " + i);
        }
      }
    }

    if (lastDot > 0) {
      sb.append(chars, 0, lastDot);
    }

    return sb.toString();
  }

  /*
   * This generates a hash for the EvidenceKey. It is compatible with equals. Note that
   * this means that it has funny rules around the address field.
   *
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    HashCodeBuilder hb = new HashCodeBuilder().append(type.ordinal());

    hb.append(address).append(domain).append(integer).append(longInteger);

    return hb.toHashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    } else if (obj == this) {
      return true;
    } else if (obj.getClass() != getClass()) {
      return false;
    }
    final EvidenceKey rhs = (EvidenceKey) obj;
    EqualsBuilder eb = new EqualsBuilder().append(type, rhs.type)
        .append(domain, rhs.domain).append(integer, rhs.integer)
        .append(longInteger, rhs.longInteger).append(address, rhs.address);

    return eb.isEquals();
  }

  /** Return a string where the final character ']' is replaced by '].' */
  private String getDomainEscaped() {
    String result = getDomain();

    if (!result.isEmpty() && result.charAt(result.length() - 1) == ']') {
      return result + ".";
    }
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();

    sb.append(type.name().toLowerCase());
    switch (type) {
      case BOGON_LIST:
      case UNDEFINED:
        break;

      case INET_ADDRESS:
        sb.append("=");
        if (address != null) {
          sb.append(address.getHostAddress());
        } else {
          sb.append("null");
        }
        break;

      case INET_ADDRESS_WITH_PORT:
        sb.append("=");
        if (address != null) {
          sb.append(address.getHostAddress());
          sb.append(":");
          sb.append(integer);
        } else {
          sb.append("null");
        }
        break;

      case AS_NUMBER:
      case ENTITY_ID:
      case INDUSTRY_ID:
        sb.append("=");
        sb.append(String.valueOf(integer));
        break;

      case DOMAIN_NAME:
        sb.append("=");
        sb.append(getDomainEscaped());
        break;

      case DOMAIN_NAME_WITH_PORT:
        sb.append("=");
        sb.append(getDomainEscaped());
        if (address != null) {
          sb.append("[").append(address.getHostAddress()).append("]");
        }

        return sb.append(":").append(integer).toString();

      case CERTIFICATE_ID:
        sb.append("=");
        sb.append(this.longInteger);
        break;

      case STRING:
        sb.append('=');
        sb.append(this.domain);
        break;

      default:
        throw new RuntimeException("toString() not implemented for Type:" + type.toString());
    }

    return sb.toString();

  }



  /**
   * Method to return a lowercase type string.
   *
   * @return type string
   */
  public String getTypeString() {
    return type.name().toLowerCase();
  }

  @Override
  public int compareTo(EvidenceKey rhs) {
    if (type != rhs.type) {
      return type.compareTo(rhs.type);
    } else if (type == KeyType.INET_ADDRESS) {
      return compare(address, rhs.address);
    }

    final CompareToBuilder ctb = new CompareToBuilder();
    switch (type) {
      case INET_ADDRESS:
        break;

      case INET_ADDRESS_WITH_PORT: {
        final int compare = compare(this.address, rhs.address);

        if (compare != 0) {
          return compare;
        }
        ctb.append(this.integer, rhs.integer);
      }
        break;

      case DOMAIN_NAME:
        ctb.append(domain, rhs.domain);
        break;

      case DOMAIN_NAME_WITH_PORT:
        ctb.append(domain, rhs.domain);
        ctb.append(this.integer, rhs.integer);

        if (ctb.toComparison() == 0) {
          final int compare = compare(this.address, rhs.address);

          if (compare != 0) {
            return compare;
          }
        }
        break;

      case CERTIFICATE_ID:
        ctb.append(longInteger, rhs.longInteger);
        break;

      case ENTITY_ID:
      case INDUSTRY_ID:
      case AS_NUMBER:
        ctb.append(integer, rhs.integer);
        break;

      case BOGON_LIST:
        break;

      case STRING:
        ctb.append(domain, rhs.domain);
        break;

      case UNDEFINED:
      default:
        break;
    }

    return ctb.toComparison();
  }



  /**
   * Method to return the content of the object in string form.
   *
   * @return String representing content
   */
  public String getValue() {
    switch (type) {
      case BOGON_LIST:
      case UNDEFINED:
        return "true";

      case INET_ADDRESS:
        if (address != null) {
          return InetAddresses.toUriString(address);
        }

        return "null";

      case INET_ADDRESS_WITH_PORT:
        if (address != null) {
          return InetAddresses.toUriString(address) + ":" + Integer.toString(integer);
        }
        return "null";

      case AS_NUMBER:
      case ENTITY_ID:
      case INDUSTRY_ID:
        return String.valueOf(integer);

      case CERTIFICATE_ID:
        return Long.toString(longInteger);

      case DOMAIN_NAME:
        return getDomain();

      case DOMAIN_NAME_WITH_PORT:
        StringBuilder sb = new StringBuilder();
        sb.append(getDomain());
        if (address != null) {
          sb.append("[").append(address.getHostAddress()).append("]");
        }

        return sb.append(":").append(integer).toString();

      case STRING:
        return domain;

      default:
        return null;
    }
  }

  /**
   * Method to return a TagValue object.
   *
   * @return TagValue object
   */
  public TagValue getTagValue() {
    switch (type) {
      case BOGON_LIST:
      case UNDEFINED:
        return new TagValue(true);

      case INET_ADDRESS:
        if (address != null) {
          return new TagValue(InetAddresses.toUriString(address));
        } else {
          return new TagValue();
        }
      case INET_ADDRESS_WITH_PORT:
        if (address != null) {
          return new TagValue(InetAddresses.toUriString(address) + ":" + Integer.toString(integer));
        } else {
          return new TagValue();
        }
      case AS_NUMBER:
      case ENTITY_ID:
      case INDUSTRY_ID:
        return new TagValue(integer);

      case CERTIFICATE_ID:
        return new TagValue(longInteger);

      case DOMAIN_NAME:
      case DOMAIN_NAME_WITH_PORT:
        return new TagValue(getValue());

      default:
        return null;
    }
  }

  /**
   * Return as many assets as possible from this object.
   * 
   * @return list of assets
   */
  public List<String> getAssets() {
    final List<String> assets = new ArrayList<>();
    if (this.hasAddress() && !StringUtils.isEmpty(this.address.getHostAddress())) {
      assets.add(this.address.getHostAddress());
    }
    if (this.hasDomain() && !StringUtils.isEmpty(getDomain())) {
      String domain = getDomain();
      if (StringUtils.startsWith(domain, "*.")) {
        domain = StringUtils.substring(domain, 2);
      }
      assets.add(domain);
    }
    return assets;
  }

  /**
   * Method to create an object EvidenceKey from a String.
   *
   * @param value string with data
   *
   * @return An EvidenceKey object
   */
  public static EvidenceKey valueOf(String value) {
    if (value == null) {
      return null;
    }

    int equalsIndex = value.indexOf('=');

    if (equalsIndex < 0) {
      return forType(value, null);
    }
    return forType(value.substring(0, equalsIndex), value.substring(equalsIndex + 1));
  }

  /**
   * EvidenceKey objects factory method.
   *
   * @param keytype type of the final object
   * @param string data for the object
   *
   * @return An EvidenceKeyObject
   */
  public static EvidenceKey forType(String keytype, String string) {
    return forType(KeyType.valueOf(keytype.toUpperCase()), string);
  }

  /**
   * Factory method to convert a string into to specific type of EvidenceKey object.
   *
   * @param keytype type of object
   * @param string data
   *
   * @return An EvidenceKey object
   */
  private static EvidenceKey forType(final KeyType keytype, String string) {
    switch (keytype) {
      case AS_NUMBER:
        return EvidenceKey.forAsNumber(Integer.parseInt(string));

      case BOGON_LIST:
        return EvidenceKey.forBogonList();

      case DOMAIN_NAME:
        return EvidenceKey.forDomain(string);

      case ENTITY_ID:
        return EvidenceKey.forEntityId(Integer.parseInt(string));

      case INDUSTRY_ID:
        return EvidenceKey.forIndustryId(Integer.parseInt(string));

      case INET_ADDRESS:
        if (string.charAt(0) == '[') {
          string = string.substring(1, string.length() - 1);
        }
        return EvidenceKey.forAddress(NumericInetAddress.forIp(string));

      case INET_ADDRESS_WITH_PORT: {
        final int colon = string.lastIndexOf(':');
        if (colon < 0) {
          throw new RuntimeException("Incorrect format for INET_ADDRESS_WITH_PORT");
        }
        String host = string.substring(0, colon);
        final String port = string.substring(colon + 1);
        if (host.charAt(0) == '[') {
          host = host.substring(1, host.length() - 1);
        }
        return EvidenceKey.forAddress(NumericInetAddress.forIp(host), Integer.parseInt(port));
      }

      case DOMAIN_NAME_WITH_PORT: {
        final int colon = string.lastIndexOf(':');
        if (colon < 0) {
          throw new RuntimeException("Incorrect format for DOMAIN_NAME_WITH_PORT");
        }
        String host = string.substring(0, colon);
        final int lsb = host.lastIndexOf('[');
        InetAddress address = null;
        final int rsb = host.lastIndexOf(']');
        if (lsb >= 0 && rsb > lsb && rsb == colon - 1) {
          // May be an IP
          try {
            address = NumericInetAddress.forIp(host.substring(lsb + 1, rsb));
            host = host.substring(0, lsb);
          } catch (IllegalArgumentException ex) {
            // Was not a valid IP
          }
        }
        final String port = string.substring(colon + 1);
        return EvidenceKey.forDomain(host, address, Integer.parseInt(port));
      }

      case CERTIFICATE_ID:
        return EvidenceKey.forCertificateId(Long.parseLong(string));

      case STRING:
        return EvidenceKey.forString(string);

      case UNDEFINED:
        return new EvidenceKey();

      default:
        return null;
    }
  }

  /**
   * Method copies to current object data from the copyFrom object.
   *
   * @param copyFrom object to copy data from
   */
  public void copyFrom(EvidenceKey copyFrom) {
    this.type = copyFrom.type;
    this.address = copyFrom.address;
    this.domain = copyFrom.domain;
    this.integer = copyFrom.integer;
    this.longInteger = copyFrom.longInteger;
  }

  /**
   * This returns a hashcode with the following properties: only the domain name is hashed
   * for domain name with port, otherwise is regular hashCode.
   */
  @Override
  public int hashCodeForTreeSet() {
    switch (getType()) {
      case DOMAIN_NAME_WITH_PORT:
        return domain.hashCode();

      default:
        return hashCode();
    }
  }

  /**
   * This returns a hashcode with the following properties: only the domain name+port is
   * hashed for domain name with port, otherwise is regular hashCode.
   */
  public int hashCodeWithoutAddressForDomain() {
    switch (getType()) {
      case DOMAIN_NAME_WITH_PORT:
        return new HashCodeBuilder().append(domain).append(integer).toHashCode();

      default:
        return hashCode();
    }
  }


  /**
   * Compare two {@link InetAddress}.
   *
   * @param first the first {@link InetAddress}, or null.
   * @param second the second {@link InetAddress}, or null.
   * @return 0 if same, &lt;0 or &gt;0 for order, and &lt;0 for shorter IPs (IPv4 vs.
   *         IPv6), and nulls first.
   */
  private static int compare(final InetAddress first, final InetAddress second) {
    if (first == second) {
      return 0;
    }
    if (first == null) {
      return -1;
    }
    if (second == null) {
      return 1;
    }
    final byte[] firstBytes = first.getAddress();
    final byte[] secondBytes = second.getAddress();
    // getAddress returns bytes in network byte order:
    // the least significant byte is at the last index
    if (firstBytes.length != secondBytes.length) {
      return firstBytes.length - secondBytes.length;
    }
    for (int i = 0; i < firstBytes.length; i++) {
      final int b1 = firstBytes[i] & 0xff;
      final int b2 = secondBytes[i] & 0xff;
      if (b1 != b2) {
        return b1 < b2 ? -1 : 1;
      }
    }
    return 0;
  }

}
