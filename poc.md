# Proof-of-Concept Evaluation Report for Apache Iceberg

## Background
Apache Iceberg is an open-source table format for storing and managing large, complex data sets in cloud data lakes that offers similar capabilities and functionality as SQL tables in traditional databases but in a fully open and accessible manner such that multiple engines (Spark, Trino, Presto, etc.) can operate on the same dataset. Iceberg provides many features such as:

* Transactional consistency between multiple applications where files can be added, removed or modified atomically, with full read isolation and multiple concurrent writes
* Full schema evolution to track changes to a table over time
* Time travel to query historical data and verify changes between updates
* Partition layout and evolution enabling updates to partition schemes as queries and data volumes change without relying on explicit partitions or physical directories
* Rollback to prior versions to quickly correct issues and return tables to a known good state
* Advanced planning and filtering capabilities for increased performance on large data volumes

## Objectives

The primary objectives of the PoC were to:

- Assess the compatibility of Iceberg with our existing data systems.
- Measure the performance improvements offered by Apache Iceberg/Spark compared to our current approach.
- Evaluate the ease of implementation and the feasability of new approaches to solve current problems.
- Identify any potential issues or limitations.

## PoC

The PoC focused mainly on the scenario of using Iceberg as an alternative storage solution for the EventStore" and most off the work is tracked on ticket [RP-148877](https://bitsight.atlassian.net/browse/RP-148877) and was roughly divided into the following tasks:* Modeling Eventstore data with Iceberg: parse and ingest data from Shodan (as one of the most complex datasources we have) and figure a way to store it on an Iceberg table ([RP-153533](https://bitsight.atlassian.net/browse/RP-153533)).
* Compare performance vs the current approach ([RP-153538](https://bitsight.atlassian.net/browse/RP-153538)).
* Validate that using Hadoop MR/Java is no limitation to Iceberg: test if it is possible (and how simple it is) to read "Iceberg eventstore" tables using Hadoop MapReduce/Java code and compare performance against the Spark ([RP-153539](https://bitsight.atlassian.net/browse/RP-153539)).
* Evaluate the usage of Trino over Iceberg ([RP-153540](https://bitsight.atlassian.net/browse/RP-153540)).
* Explore ways to increase the bulk read selectivity of the EventStore ([RP-153541](https://bitsight.atlassian.net/browse/RP-153541)).
* Evaluate alternative ways to perform Entity attribution  ([RP-153542](https://bitsight.atlassian.net/browse/RP-153542), [RP-154070](https://bitsight.atlassian.net/browse/RP-154070)).

The results of the PoC were as follows:

* Raw data from Shodan was successfully imported and stored in an Iceberg table, demonstrating Iceberg's ability to handle diverse data sources.
* Spark/Iceberg performance outperformed the current aggregator implementation, resulting in faster query times and improved resource utilisation.
* Hadoop MapReduce was able to read data from Iceberg tables, confirming compatibility with the Hadoop ecosystem.
 * Trino with Iceberg in AWS demonstrated good performance and seamless integration, providing a flexible and scalable analytics platform.
* The index-like schema in Iceberg tables didn't provide much benefit in terms of selectivity, and my advice is to abandon this approach.
* Entity mapping using IP and domain matching was successfully performed using SQL-like JOIN operations.


Most of these tasks were handled by Iceberg/Spark with great success and will not be analysed here (see the tickets above), but I want to focus a bit on the task that didn't have an amazing result, the one related to "bulk read selectivity of the EventStore".

One of the current problems we face in the data pipeline is the huge amount of data that is read and then discarded in the "read" stage of EntityJoin, currently we have no choice but to read all the data from a specified "start date" and only then filter out the irrelevant data.

To efficiently answer a query request, the goal should be to read as little data as possible. Iceberg facilitates techniques such as partitioning, pruning, and min/max filtering to skip data files that do not match the query predicate. You can read [more about all the techniques here](https://iceberg.apache.org/docs/latest/performance/).

Using column-level statistics from Iceberg manifest files allows query engines to skip data files, resulting in faster queries. However, the physical layout of the data is very important for this to be effective. In addition to partitioning, a common strategy to optimise this scenario is to sort the data within a file based on the metrics. By sorting the entire dataset, the data is rearranged so that each data file ends up with a unique range of values for the specific column on which it is sorted. And, of course, you can add other columns to the already sorted column in a hierarchical fashion.

The problem we have is that from one day to the next there are potentially several thousand new entities, each with associated IP/domain/date ranges that span multiple partitions.
I have not been able to create a partition/sorting scheme that prevents less than 70% of the data from being read, i.e. the best result still had to read/touch 70% of the files. It's a better result than the current approach, but not by an amazing margin.

## Conclusion

It should be noted that some of Iceberg features, such as time travel and ACID properties, were not tested extensively due to time constraints but nevertheless this evaluation suggests that Apache Iceberg can be a valuable addition to our data infrastructure, providing improved performance, flexibility, and reliability. The compatibility with our existing tools and frameworks, combined with its ease of implementation and maintenance, makes Iceberg a promising option for future data projects. However, additional testing and evaluation are recommended to ensure Iceberg's performance and scalability under production-level data volumes.